<?php

function get_content() { ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-8 offset-2">
				<h1>Registration Form</h1>
				<form method="post" action="../controllers/registration.php">
					<div class="form-group row">
						<label for="fName" class="col-3 text-right">
							First Name: 
						</label>
						<input type="text" class="form-control col-9" id="fName" name="fName">
						<span style="color: red" id="fNameError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="lName" class="col-3 text-right">
							Last Name: 
						</label>
						<input type="text" class="form-control col-9" id="lName" name="lName">
						<span style="color: red" id="lNameError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="email" class="col-3 text-right">
							Email: 
						</label>
						<input type="text" class="form-control col-9" id="email" name="email">
						<span style="color: red" id="emailError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="password" class="col-3 text-right">
							Password: 
						</label>
						<input type="password" class="form-control col-9" id="password" name="password">
						<span style="color: red" id="passwordError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="confirmPwd" class="col-3 text-right">
							Confirm Password: 
						</label>
						<input type="password" class="form-control col-9" id="confirmPwd" name="confirmPwd">
						<span style="color: red" id="cPwdError" class="col-9 offset-3"></span>
					</div>
					<button type="submit" id="submitBtn" class="btn btn-primary" disabled>Submit</button>
				</form>
			</div>
		</div>
	</div>
<?php };

require_once "../partials/template.php";
?>

<script type="text/javascript">
	const email = document.querySelector("#email");
	const password = document.querySelector("#password");
	const confirmPwd = document.querySelector("#confirmPwd");
	const passwordError = document.querySelector("#passwordError");

	const inputs = document.querySelectorAll("input.form-control");

	function checkEmail() {
		return fetch('../controllers/check_email.php?email=' + email.value).then(
			function(res){
				return res.text();
			}).then(function(data){
				let flag = false;
				if (data == "not available") {
					email.nextElementSibling.innerHTML = "email is already registered";
					flag = true;
				}
				return flag; 
			});
	}
	inputs.forEach(function(input){
		input.addEventListener("input", function(){
			if (this.value == ""){
				this.nextElementSibling.innerHTML = "This field is required";
			}else {
				this.nextElementSibling,innerHTML = "";
			}
			checker();
		});
	});
	function checker() {
		let errorFlag = false;
		inputs.forEach(function(inp){
			if (inp.value == "") {
				errorFlag = true;
			}
		});

		let pwdVal = password.value;
		let cpwdVal = confirmPwd.value;
		if((pwdVal !== "") && (pwdVal !== cpwdVal)){
			errorFlag = true;
		}

		checkEmail().
		then(function (data) {
			if (!data && !errorFlag) {
				document.querySelector("#submitBtn").disabled = false;
			}else{
				document.querySelector("#submitBtn").disabled = true;
			}
		})
	}


</script>