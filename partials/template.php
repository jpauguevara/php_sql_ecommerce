<?php

	if(session_status()==PHP_SESSION_NONE) {
		session_start();
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>B23 Store</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="semantic/dist/semantic.min.css">
	<script
	  src="https://code.jquery.com/jquery-3.1.1.min.js"
	  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	  crossorigin="anonymous"></script>
	<script src="semantic/dist/semantic.min.js"></script>
</head>
<body>

	<div class="container">

		<?php require_once "header.php"; ?>
		<div class="container-fluid" id="main">
			<?php get_content(); ?>
		</div>
		<?php require_once "footer.php"; ?>


	</div>


</body>
</html>