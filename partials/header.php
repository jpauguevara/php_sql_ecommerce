

<div class="ui secondary  menu">

  <?php

      if(!isset($_SESSION['userID'])) {
          echo '
            <a class="active item">Home</a>
            <a class="item">Products</a>
            <a class="item">Login</a>
            <div class="right menu">
                <div class="item">
                    <div class="ui icon input">
                        <input type="text" placeholder="Search...">
                        <i class="search link icon"></i>
                    </div>
                </div>
                <a class="ui item">Register</a>
            </div>
          ';
        }else {
            if($_SESSION['isAdmin'] == 'true') {
              echo '
                  <a class="active item">Home</a>
                  <a class="item">Add Products</a>
                  <div class="right menu">
                      <div class="item">
                          <div class="ui icon input">
                              <input type="text" placeholder="Search...">
                              <i class="search link icon"></i>
                          </div>
                      </div>
                      <a class="ui item">Logout</a>
                  </div>
                ';
              }else if($_SESSION['isAdmin'] == 'false') {
                  echo '
                      <a class="active item">Home</a>
                      <a class="item">Cart</a>
                      <a class="item">Transactions</a>
                      <div class="right menu">
                          <div class="item">
                              <div class="ui icon input">
                                  <input type="text" placeholder="Search...">
                                  <i class="search link icon"></i>
                              </div>
                          </div>
                          <a class="ui item">Logout</a>
                      </div>
                  ';
              }
        }  

    ?>
</div>